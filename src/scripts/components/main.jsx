/** @jsx React.DOM */

var TransformerApp = require('./TransformerApp');
var React = require('react');
var {DefaultRoute, Route, Routes} = require('react-router');

React.renderComponent((
  <Routes location="history">
    <Route path="/" handler={TransformerApp}>
    </Route>
  </Routes>
), document.getElementById('content'));
