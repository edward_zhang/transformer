'use strict';

describe('Main', function () {
  var TransformerApp, component;

  beforeEach(function () {
    var container = document.createElement('div');
    container.id = 'content';
    document.body.appendChild(container);

    TransformerApp = require('../../../src/scripts/components/TransformerApp.jsx');
    component = TransformerApp();
  });

  it('should create a new instance of TransformerApp', function () {
    expect(component).toBeDefined();
  });
});
